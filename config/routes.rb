Rails.application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Ckeditor::Engine => '/ckeditor'

  #get 'contacts' => 'contacts#new', as: :contacts
  #post 'contacts' => 'contacts#create', as: :create_contacts
  #get 'contacts/sent' => 'contacts#sent', as: :contacts_sent

  #get 'search' => 'search#index', as: :search

  # resources :news, only: [:index, :show]
  # namespace api do
  #   resources :status, only: :index
  # end

  get 'users/edit/index' => 'api/status#index', as: :show_type_articles, method: :get
  namespace :api do
    resources :article
  end
  resources :users, only: [:update, :edit, :destroy]
  resources :articles do
    member do
      post :edit_rating
    end
  end
  get '*slug' => 'pages#show'
  resources :pages, only: [:show]
  root to: 'articles#index'
end
