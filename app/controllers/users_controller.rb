class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end

  def update
    @user_update = User.find(params[:id])
     if @user_update.update(user_params)
       flash[:notice] = 'Данные обновлены успешно'
       redirect_to edit_user_path(params[:id])
     else
       flash[:error] = @user_update.errors.full_messages.join(', ')
       redirect_to edit_user_path(params[:id])
     end
  end

  def edit
    @user = User.find(params[:id])
    @articles = Article.where(user_id: params[:id] )
    @last_five = Article.where(user_id: params[:id] ).order(updated_at: :desc).limit(5)
  end

  def destroy
    if User.find(params[:id]).destroy
      flash[:notice] = 'Пользователь удален успешно'
      redirect_to root_path
    else
      flash[:error] = 'Пользователь не удален'
      redirect_to edit_user_path(params[:id])
  end
end



  private

def article_params
  params.require(:article).permit(:user_id)
end

def user_params
  params.require(:user).permit(:fname, :lname, :email, :password, :password_confirmation)
end

end
