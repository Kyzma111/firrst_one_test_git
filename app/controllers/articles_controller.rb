class ArticlesController < ApplicationController
  def index
#@test = user_session
#binding.pry

  #if user_signed_in? and current_user.role == "user"
    @articles = Article.where(status: "accepted").page(params[:page]).per(10)
#  else
#    redirect_to "/admin"
#  end
  #  @article = Article.find(params[:id])
    # @article_pag=Kaminari.paginate_array(@all_article)


#@article_pag=Kaminari.paginate_array(@all_article).page(1).per(10)
  end
  def new
   @article = Article.new
   @article.images.build
   @category = Category.all
  end

  def create
      article = Article.create(article_params)
      if article.errors.any?
        flash[:error] = article.errors.full_messages.join(",")
       redirect_to new_article_path
      else
        flash[:notice] = 'Статья создана успешно'
        redirect_to article_path(article.id)
     end
end

  def edit
    @article = Article.find(params[:id])
    @category = Category.all
    @user = User.all
  end

  def show
    @article = Article.find(params[:id])
    @article_cat = Article.where(category_id: @article.category_id).sample(3)
    @raiting =  @article.points.count > 0 ? @article.points.pluck(:rating).sum.to_f / @article.points.count : 0
    @article.update_attribute(:visits, @article.visits.to_i + 1)
    @point = Point.new
  end
  def user_page
    @articles = Article.where( user_id: params[:user_id])
  end

  def category
      @article_cat_all = Article.where(category_id: params[:category_id])
  end

  def update
    @article_update = Article.find(params[:id])
     if @article_update.update(article_params)
       flash[:notice] = 'Статья обновлена успешно'
       redirect_to article_path(params[:id])
     else
       flash[:error] = @article_update.errors.full_messages.join(', ')
       redirect_to article_path(params[:id])
     end
  end

  def destroy
    if Article.find(params[:id]).destroy
      flash[:notice] = 'Статья удалена успешно'
      redirect_to root_path
    else
      flash[:error] = 'Статья не удалена'
      redirect_to article_path(params[:id])
    end
  end

  def edit_rating
    if Point.create(params[:point].permit!)
      flash[:notice] = "Спасибо за оценку."
      redirect_to article_path(params[:point][:article_id])
    else
      flash[:error] = "Оценка не учтена."
      redirect_to article_path(params[:point][:article_id])
    end
  end


  private

  def article_params
    params.require(:article).permit(:user_id ,:title, :description, :category_id, images_attributes:[:image])
  end




end
