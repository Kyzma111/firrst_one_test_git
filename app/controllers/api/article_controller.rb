class Api::ArticleController < ApplicationController
  def index
    page = params[:page] || 1
    category_id = params[:category_id] || Category.first.id
    article = Article.where(category_id: category_id).page(page).per(3)
    render json: article
  end
end
