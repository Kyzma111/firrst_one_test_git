class Api::StatusController < ApplicationController
  def index
    @articles = Article.where(user_id: current_user.id, status: params[:type]).page(params[:page]).per(5)
    render partial: "articles/blocks/index-article", collection: @articles, as: :item

  end
end
