class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :category, :visits
  belongs_to :user
  has_many :tags
  has_many :points

  def category
    object.category.title
  end

end
