class TagSerializer < ActiveModel::Serializer
  attributes :id, :article_id, :text
end
