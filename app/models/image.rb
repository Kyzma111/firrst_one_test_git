# == Schema Information
#
# Table name: images
#
#  id                 :bigint(8)        not null, primary key
#  name               :string
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :bigint(8)
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Image < ApplicationRecord
  #locale :ru
  belongs_to :imageable, polymorphic: true

  has_attached_file :image,
                    styles: { medium: "300x300>",
                              thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  rails_admin do
    field :name
    field :image
  end
end
