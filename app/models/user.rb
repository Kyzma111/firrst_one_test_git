# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  fname                  :string
#  lname                  :string
#  role                   :string
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :timeoutable, :trackable and :omniauthable
#before_action :authenticate_user!
  before_save :default_role
  devise :database_authenticatable,  :lockable,
         :recoverable, :rememberable, :validatable,:registerable

  has_many :articles, dependent: :destroy
  has_one :image, as: :imageable
  has_many :points


  def default_role
    if role.nil?
      self.role="user"
    end
  end

  def admin?
    self.role == "admin"
  end

  def moder?
    self.role == "moder"
  end

  def epass
    self.encrypted_password
  end
  def pass
    self.current_password
  end

  def full_name
    [self.fname ,self.lname].join(" ")
  end

  def article_points?(article_id)
      Point.where(user_id: self.id, article_id: article_id).empty?
  end

  rails_admin do
    list do
      field :email
      field :full_name
      field :role
      field :image
    end
  end
end
