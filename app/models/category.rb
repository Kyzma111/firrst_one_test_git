# == Schema Information
#
# Table name: categories
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  color      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Category < ApplicationRecord
  has_one :article

rails_admin do
  list do
    field :title
    field :color
  end
  edit do
    field :title
    field :color, :color
  end
end






end
