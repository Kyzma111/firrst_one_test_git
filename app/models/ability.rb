class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user
    unless user.nil?
    #  binding.pry
      if user.admin?
        admin_ui
      elsif user.moder?
        moder_ui
      else
        user_ui
      end
    end
  end

  def admin_ui
    can :read, :all
    can :manage, :all
    cannot :destroy, Menu
    cannot :update, Menu
    can :access, :rails_admin   # grant access to rails_admin
    can :dashboard              # grant access to the dashboard
  end
  def moder_ui
    can :read, Article
    can :manage, Article
    cannot :destroy, Menu
    cannot :update, Menu
    can :access, :rails_admin   # grant access to rails_admin
    can :dashboard
    can :update, Article, user: @user
  end
  def user_ui
    can :read, :all
    can :update, Article, user: @user
  end
end
