# == Schema Information
#
# Table name: articles
#
#  id          :bigint(8)        not null, primary key
#  title       :string
#  user_id     :bigint(8)
#  description :text
#  category_id :bigint(8)
#  status      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Article < ApplicationRecord
  #include ActiveModel::Serialization
  belongs_to :user, dependent: :destroy
  has_many :tags, dependent: :destroy
  belongs_to :category
  has_many :images, as: :imageable, dependent: :destroy
  has_many :points
  accepts_nested_attributes_for :images, allow_destroy: true
  # validates :name, :length { :minimum 1, to_short: "name must be %{count} symbols" }
  validates :title, length: { minimum: 2 }
  validates :description, presence: true
  validates :category_id, numericality: true
#locale :ru
  state_machine :status, :initial => :inprocess do
    event :accept do
      transition [:inprocess, :disabled] => :accepted
    end
    event :disable do
      transition [:inprocess, :accepted] => :disabled
    end
    event :inprocess do
      transition [:accepted,:disabled] => :inprocess
    end
    after_transition any => :accepted do |article|
      UserMailer.accept_email(article.title, article.user.email, article.user.full_name).deliver_now
    end
    after_transition any => :disabled do |article|
      UserMailer.disable_email(article.title, article.user.email, article.user.full_name).deliver_now
    end
    after_transition any => :inprocess do |article|
      UserMailer.innprocess_email(article.title, article.user.email, article.user.full_name).deliver_now
    end
  end
  rails_admin do
    list do
      field :status, :state
      field :category
      field :user
      field :title
      field :description
      field :category
    end
   edit do
     field :status, :state
     field :title
     field :user
     field :description, :ckeditor
     field :category
  end
  state({
  events: {accept: 'btn-success', disable: 'btn-danger', inprocess: 'btn-warning'},
  states: {accept: 'label-info', accepted: 'label-success', disabled: 'label-danger', inprocess: 'label-warning'}
  #  disable: [:dead] # disable some events so they are not shown.
  })
  end
end
