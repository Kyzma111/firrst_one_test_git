# == Schema Information
#
# Table name: tags
#
#  id         :bigint(8)        not null, primary key
#  text       :string
#  article_id :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tag < ApplicationRecord
  belongs_to :article

rails_admin do
  list do
    field :text
    field :article
  end
end



end
