class UserMailer < ActionMailer::Base
  default from: 'notifications@example.com'

  def accept_email(title, email, name)
    @title = title
    @name = name
    mail(to: email, subject: 'Статья принята')
  end
  def disable_email(title, email, name)
    @title = title
    @name = name
    mail(to: email, subject: 'Статья отклонена')
  end
  def innprocess_email(title, email, name)
    @title = title
    @name = name
    mail(to: email, subject: 'Статья принята в обработку')
  end
end
